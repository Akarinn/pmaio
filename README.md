# **PM-AIO: An Effective Asynchronous I/O System for Persistent Memory**
## **Introduction**
PM-AIO, is a modified version of Linux Kernel-AIO that achieves an effective async I/O path on PM file systems, on the basis of retaining original system call interfaces of Linux Kernel-AIO.


The main modules are:

(1) Fine-grained read/write lock mechanism

(2) Asynchronous request scheduling organization queue

(3) Asynchronous I/O request execution module

The above modules are implemented in files namely inode.c and super.c.
## **Author**
PM-AIO was written by a group of School of Computer Science, South China Normal University. They investigated the pseudo-async I/O problem in PM file systems, thus aiming at creating real async IO.
## **Downloading**
https://gitlab.com/dingdly/pmaio

After some bugs are fixed, we will successively upload the implementation of PM-AIO in PMFS and NOVA.

## **Building**
To build PM-AIO, simply run these commands.
~~~
#make
#./setup-pmaio.sh
~~~

## **Current limitations**

| File Systems | Kernel Version |
| :------: | :------: |
| PMFS | 4.15.7 |
| NOVA | 5.1 |

PMFS utilizes the latest ordered journal mode embodied in the open-source Linux kernel version 4.15.7.

NOVA is developed with the kernel version 5.1, exploiting the in-place update mode as the default update mode.
	




